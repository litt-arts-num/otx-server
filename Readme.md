# OTX server Docker

## Installation

```
git clone ...
cp .env.dist .env
vi .env
docker-compose up -d --build
```

```
docker-compose exec db bash
mysql -u root -p'root' opentext < /tmp/init.sql
exit
```

```
docker-compose exec otx bash
cp otx.config.xml.sample otx.config.xml
vi otx.config.xml
php install/create_user.php otx-user otx-password
chown -R www-data:www-data ./cache
```

Pour l'utilisation avec lodel, il faut configurer dans lodelconfig.php de votre installation lodel
la connexion à l'interface du serveur OTX :
```
$cfg['otxurl']      = "http://myotxserver.com/";
$cfg['otxusername'] = "otx-user";
$cfg['otxpasswd']   = "otx-password";
```
